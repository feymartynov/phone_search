# PhoneSearch

Demo: https://frozen-tor-30819.herokuapp.com

### Running in development mode

```
bundle install
npm install

npm run watch &
bundle exec rackup
```

### Testing

```
bundle exec rspec
```
