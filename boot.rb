require 'json'
require 'yaml'
require 'uri'
require 'open-uri'
require 'rubygems'
require 'bundler'

ENV['RACK_ENV'] ||= 'development'
Bundler.require(:default, ENV['RACK_ENV'])

pattern = File.join(File.dirname(__FILE__), 'lib/**/*.rb')
Dir[pattern].sort.each { |f| require f }
