import angular from 'angular';
import ngRoute from 'angular-route';

import SearchController from './controllers/search_controller';
import DeviceController from './controllers/device_controller';

angular.module('app.controllers', [])
  .controller('app.searchController', SearchController)
  .controller('app.deviceController', DeviceController);

function config($routeProvider) {
  const searchPage = {
    templateUrl: 'views/search.html',
    controller: 'app.searchController'
  };

  const devicePage = {
    templateUrl: 'views/device.html',
    controller: 'app.deviceController'
  };

  $routeProvider
    .when('/', searchPage)
    .when('/search', searchPage)
    .when('/search/:query', searchPage)
    .when('/device/:provider/:id', devicePage)
    .otherwise({redirectTo: '/'});
}

config.$inject = ['$routeProvider'];

angular
  .module('app', [ngRoute, 'app.controllers'])
  .config(config)
  .config(['$locationProvider', function ($locationProvider) {
    $locationProvider.html5Mode(true);
  }]);
