class DeviceController {
  constructor($scope, $routeParams) {
    $scope.device = null;
    $scope.error = null;

    function fetchDevice(provider, id) {
      const url = `/api/fetch?provider=${encodeURIComponent(provider)}&id=${encodeURIComponent(id)}`;
      const xhr = new XMLHttpRequest();
      xhr.open('GET', url, true);

      xhr.onload = () => {
        switch (xhr.status) {
          case 200:
            const json = JSON.parse(xhr.responseText);
            return $scope.$apply(() => $scope.device = json.device_info);
          case 404:
            return $scope.$apply(() => $scope.error = 'Device not found');
          default:
            console.error('Fetch request error', xhr);
        }
      };

      xhr.send();
    }

    fetchDevice($routeParams.provider, $routeParams.id);
  }
}

DeviceController.$inject = ['$scope', '$routeParams'];
export default DeviceController;
