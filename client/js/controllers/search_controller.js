class SearchController {
  constructor($scope, $routeParams, $location) {
    $scope.query = $routeParams.query;
    $scope.results = [];
    $scope.searchPerformed = false;

    $scope.handleSubmit = () => {
      if ($scope.query && $scope.query.length > 0) {
        $location.path(`/search/${encodeURIComponent($scope.query)}`);
      }
    };

    function readNDJsonStream(xhr, callback) {
      var cursor = 0;

      xhr.onprogress = () => {
        if (xhr.readyState < 3) return;

        const lastNlPosition = xhr.responseText.lastIndexOf("\n");
        if (lastNlPosition <= cursor) return;

        const lines = xhr.responseText.slice(cursor, lastNlPosition).split("\n");

        for (const line of lines) {
          if (line.length > 0) callback(JSON.parse(line));
        }

        cursor = lastNlPosition;
      };
    }

    function search() {
      $scope.searchPerformed = false;
      $scope.results = [];

      let xhr = new XMLHttpRequest();
      const url = `/api/search?query=${encodeURIComponent($scope.query)}`;

      xhr.open('GET', url, true);

      readNDJsonStream(xhr, result => {
        $scope.$apply(() => $scope.results.push(result));
        $scope.searchPerformed = true;
      });

      xhr.send();
    }

    if ($scope.query) search();
  }
}

SearchController.$inject = ['$scope', '$routeParams', '$location'];
export default SearchController;
