require_relative './boot'

map '/api' do
  config = YAML.load_file('config.yml')
  run PhoneSearch::App.new(config)
end

map '/' do
  static_files = Dir['./public/**/*'].map { |f| f.gsub(%r{\A\./public}, '')}
  rack_file = Rack::File.new('./public')

  run -> (env) do
    # Fall back to index.html to allow entering the app not only from /
    unless static_files.include?(env['PATH_INFO'])
      env['PATH_INFO'] = '/index.html'
    end

    rack_file.call(env)
  end
end
