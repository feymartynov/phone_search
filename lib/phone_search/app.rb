module PhoneSearch
  class App
    JSON_HEADER = {'Content-Type' => 'application/json'}

    ROUTES = {
      %w(GET /search) => :search,
      %w(GET /fetch) => :fetch }

    def initialize(config)
      @providers = config[:providers].inject({}) do |memo, (name, klass)|
        memo[name] = Object.const_get(klass).new
        memo
      end

      @search = Search.new(@providers)
    end

    def call(env)
      request = Rack::Request.new(env)
      route = ROUTES[[request.request_method, request.path_info]]

      if route
        send(route, request)
      else
        [404, JSON_HEADER, [{ error: 'Not found' }.to_json]]
      end
    end

    private

    def search(request)
      results = Enumerator.new do |y|
        @search.search(request.params['query']) { |result| y << result }
      end

      [200, JSON_HEADER, results.lazy.map { |r| "#{r.to_json}\n" }]
    end

    def fetch(request)
      provider = @providers[request.params['provider']]
      result = provider&.fetch(request.params['id'])

      if result
        [200, JSON_HEADER, [{ device_info: result }.to_json]]
      else
        [404, JSON_HEADER, [{ error: 'Not found' }.to_json]]
      end
    end
  end
end
