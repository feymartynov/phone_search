module PhoneSearch
  module Providers
    class GsmArena
      def search(query)
        url = "http://www.gsmarena.com/results.php3?sName=#{URI.escape(query)}"
        html = Nokogiri::HTML(open(URI(url)))

        html.css('.makers>ul>li').each do |li|
          id = li.css('a').first['href'].sub(%r{\.php\z}, '')
          name = li.css('strong>span').first.inner_html.to_s.sub('<br>', ' ')
          image_url = li.css('img').first['src']
          yield id: id, name: name, image_url: image_url
        end
      end

      def fetch(id)
        url = "http://www.gsmarena.com/#{URI.escape(id)}.php"
        html = Nokogiri::HTML(open(url))

        { name: html.css('.specs-phone-name-title').first.text,
          image_url: html.css('.specs-photo-main img').first['src'],
          specs: parse_specs(html) }
      end

      private

      def parse_specs(html)
        html.css('#specs-list>table').map do |table|
          { section: table.css('th').text,
            properties: parse_properties(table) }
        end
      end

      def parse_properties(table)
        table.css('tr').map do |tr|
          { property: tr.css('td.ttl').text,
            value: tr.css('td.nfo').text }
        end
      end
    end
  end
end
