module PhoneSearch
  class Search
    def initialize(providers)
      @providers = providers
    end

    def search(query)
      futures = @providers.map do |name, klass|
        Concurrent.future do
          klass.search(query) do |result|
            yield result.merge(provider: name)
          end
        end
      end

      Concurrent.zip(*futures).value!
    end
  end
end
