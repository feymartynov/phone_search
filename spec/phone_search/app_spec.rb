require 'support/dummy_provider'

RSpec.describe PhoneSearch::App do
  include Rack::Test::Methods

  let(:config) { YAML.load(<<-YAML) }
    :providers:
      dummy: DummyProvider
  YAML

  def app
    PhoneSearch::App.new(config)
  end

  describe 'GET /search' do
    it 'should return search results' do
      get '/search?query=test'

      expect(last_response.status).to eq(200)
      first_line = JSON.parse(last_response.body.each_line.first)

      expect(first_line).to eq(
        'provider' => 'dummy',
        'id' => 0,
        'name' => 'dummy0',
        'query' => 'test')
    end
  end

  describe 'GET /fetch' do
    it 'should return device info' do
      get '/fetch?provider=dummy&id=123'

      expect(last_response.status).to eq(200)

      expect(JSON.parse(last_response.body)).to eq(
        'device_info' => {
          'id' => '123',
          'name' => 'dummy123',
          'specs' => { 'property' => 'value' }})
    end

    it 'should return 404 when not found' do
      get '/fetch?provider=unknown&id=0'
      expect(last_response.status).to eq(404)
    end
  end

  it 'should return 404 on missing route' do
    get '/nevermind'
    expect(last_response.status).to eq(404)
  end
end
