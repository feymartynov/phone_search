RSpec.describe PhoneSearch::Providers::GsmArena do
  subject(:provider) { described_class.new }

  describe '#search', vcr: true do
    it 'should scrape search results' do
      results = []
      provider.search('iPhone') { |result| results << result }

      expect(results).to match array_including(
        id: 'apple_iphone_7-8064',
        name: 'Apple iPhone 7',
        image_url: 'http://cdn2.gsmarena.com/vv/bigpic/apple-iphone-7r4.jpg')

      expect(results.size).to eq(43)
    end
  end

  describe '#fetch', vcr: true do
    subject(:device_info) { provider.fetch('apple_iphone_7-8064') }

    it 'should scrape the device info' do
      expect(device_info).to match hash_including(
        name: 'Apple iPhone 7',
        image_url: 'http://cdn2.gsmarena.com/vv/bigpic/apple-iphone-7r4.jpg',
        specs: array_including(
          hash_including(
            section: 'Network',
            properties: array_including(
              property: 'Technology',
              value: 'GSM / CDMA / HSPA / EVDO / LTE'))))
    end
  end
end
