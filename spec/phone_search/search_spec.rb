require 'support/dummy_provider'

RSpec.describe PhoneSearch::Search, '#search' do
  subject(:search) do
    described_class.new(
      'dummy1' => DummyProvider.new,
      'dummy2' => DummyProvider.new)
  end

  it 'should yield results' do
    results = []
    search.search('something') { |result| results << result }

    expect(results).to match array_including(
      { id: 0, name: 'dummy0', query: 'something', provider: 'dummy1' },
      { id: 1, name: 'dummy1', query: 'something', provider: 'dummy1' },
      { id: 0, name: 'dummy0', query: 'something', provider: 'dummy2' },
      { id: 1, name: 'dummy1', query: 'something', provider: 'dummy2' })
  end
end
