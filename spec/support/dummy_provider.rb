class DummyProvider
  def search(query)
    2.times do |id|
      yield id: id, name: "dummy#{id}", query: query
    end
  end

  def fetch(id)
    { id: id, name: "dummy#{id}", specs: { property: 'value' } }
  end
end
